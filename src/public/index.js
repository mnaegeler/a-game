const socket = io('https://3001-d4a67f7e-67cb-4a2b-983a-788eb16721a3.ws-us02.gitpod.io/')

const Config = {
    screenWidth: 500,
    screenHeight: 500,
    itemWidth: 20,
    itemHeight: 20,
    playerColor: 'rgba(0, 0, 0, .2)',
    currentPlayerColor: 'rgba(0, 0, 0, .4)',
    fruitColor: 'rgba(0, 255, 0, .5)'
}

const Data = (function () {
    let players = []
    let fruits = []
    let logs = []

    function getPlayers () {
        return players
    }

    function setPlayers (value) {
        players = value
    }

    function getFruits () {
        return fruits
    }

    function setFruits (value) {
        fruits = value
    }

    function getLogs () {
        return logs
    }

    function setLogs (value) {
        logs = value
    }

    return {
        getPlayers,
        setPlayers,
        getFruits,
        setFruits,
        getLogs,
        setLogs
    }
})()

const Canvas = (function () {
    const screen = document.getElementById('game-screen')
    const context = screen.getContext('2d')
    const height = screen.height
    const width = screen.width
    
    function render () {
        context.clearRect(0, 0, width, height)

        Data.getPlayers().forEach(function (player) {
            context.fillStyle = Config.playerColor;

            if (player.id === Game.getCurrentPlayerId()) {
                context.fillStyle = Config.currentPlayerColor;
            }

            context.fillRect(player.position[0], player.position[1], Config.itemWidth, Config.itemHeight)
        })

        Data.getFruits().forEach(function (fruit) {
            context.fillStyle = Config.fruitColor;
            context.fillRect(fruit.position[0], fruit.position[1], Config.itemWidth, Config.itemHeight)
        })

        requestAnimationFrame(render)
    }

    return {
        render
    }
})()

const KeyboardListener = (function () {
    document.addEventListener('keydown', onKeyDown)

    function onKeyDown ($event) {
        Game.dispatchAction($event.key)
    }

    return {}
})()

const Game = (function () {
    let currentPlayer = null

    const actions = {
        ArrowUp: function () {
            movePlayer('up')
        },
        ArrowDown: function () {
            movePlayer('down')
        },
        ArrowRight: function () {
            movePlayer('right')
        },
        ArrowLeft: function () {
            movePlayer('left')
        },
    }

    function movePlayer (direction) {
        socket.emit('movePlayer', { playerId: currentPlayer.id, direction });
    }

    function dispatchAction (action) {
        if (actions[action]) {
            actions[action]()
        }
    }

    function setCurrentPlayer (playerData) {
        currentPlayer = playerData
    }

    function getCurrentPlayerId () {
        return currentPlayer ? currentPlayer.id : null
    }

    return {
        dispatchAction,
        setCurrentPlayer,
        getCurrentPlayerId
    }
})()

const Logs = (function () {
    const container = document.getElementById('logs')

    function render () {
        const logList = []
        Data.getLogs()
            .forEach(function (log) {
                logList.push([
                    `Player ${log.playerId} ${log.action}`,
                    log.data ? JSON.stringify(log.data) : ''
                ].join(' '))
            })

        container.innerHTML = logList.join('<br>')
    }

    return {
        render
    }
})()

;(function () {
    socket.on('updateGameData', function (gameData) {
        Data.setFruits(gameData.fruits)
        Data.setPlayers(gameData.players)
        Data.setLogs(gameData.logs)

        Logs.render()
    })

    socket.on('setCurrentPlayer', function (player) {
        Game.setCurrentPlayer(player)
    })

    Canvas.render()
})()