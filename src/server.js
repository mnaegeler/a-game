const uuidv4 = require('uuid/v4')
const express = require('express')
const app = express()

const server = require('http').createServer();
const io = require('socket.io')(server);

const {state, actions} = require('./store')

const port = process.env.PORT || 3000

app.use(express.static('src/public'))
app.get('/', (req, res) => res.sendFile('./public/index.html'))

io.on('connection', client => {
    const player = {
        id: uuidv4(),
        position: [0, 0]
    }

    actions.addPlayer(player)

    client.playerId = player.id

    client.emit('setCurrentPlayer', player)
    io.emit('updateGameData', { players: state.players, fruits: state.fruits, logs: state.logs })
    
    client.on('movePlayer', ({ playerId, direction }) => {
        const player = state.players.find(player => player.id === playerId)
        if (!player) {
            return
        }

        actions.movePlayer({ player, direction })

        io.emit('updateGameData', { players: state.players, fruits: state.fruits, logs: state.logs })
    });

    client.on('disconnect', () => {
        actions.removePlayer(client.playerId)
    });
});

server.listen(3001)
app.listen(port)