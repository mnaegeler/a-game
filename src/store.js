const state = {
    players: [],
    fruits: [],
    logs: []
}

const actions = {
    addPlayer (player) {
        state.players.push(player)
        state.logs.push({ playerId: player.id, action: 'connected' })
    },

    removePlayer (playerId) {
        const index = state.players.findIndex(player => player.id === playerId)
        if (index !== undefined) {
            state.players.splice(index, 1)
        }

        state.logs.push({ playerId, action: 'disconnected' })
    },

    movePlayer ({ player, direction }) {
        const oldPosition = [ player.position[0], player.position[1] ]

        if (direction === 'up') {
            const intendedPosition = player.position[1] - 20
            if (intendedPosition >= 0) {
                player.position[1] = intendedPosition
            }
        } else if (direction === 'down') {
            const intendedPosition = player.position[1] + 20
            if (intendedPosition < 500) {
                player.position[1] = intendedPosition
            }
        } else if (direction === 'right') {
            const intendedPosition = player.position[0] + 20
            if (intendedPosition < 500) {
                player.position[0] = intendedPosition
            }
        } else if (direction === 'left') {
            const intendedPosition = player.position[0] - 20
            if (intendedPosition >= 0) {
                player.position[0] = intendedPosition
            }
        }

        state.logs.push({ playerId: player.id, action: 'moved', data: { direction, oldPosition, newPosition: player.position } })
    } 
}

module.exports = {
    state,
    actions
}